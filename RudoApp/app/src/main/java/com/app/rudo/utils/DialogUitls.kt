package com.app.rudo.utils

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AlertDialog

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class DialogUitls() {

    companion object {
        fun showDialog(
            context: Context,
            titel: String,
            message: String,
            yesButton:Boolean?,
            noButton:Boolean?,
            listner: OnClickDialogItemImpl
        ) {
            var dialogAleart: Dialog? = null
            val builder = AlertDialog.Builder(context)
            dialogAleart = builder.create()
            builder.setTitle(titel)
            builder.setMessage(message)

            if (yesButton == true) {
                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    listner.onClickYes(dialogAleart)
                }
            }
            if(noButton == true) {
                builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    listner.onClickNo(dialogAleart)
                }
            }

            /*builder.setNeutralButton("Maybe") { dialog, which ->

            }*/
            builder.show()
        }
    }

    interface OnClickDialogItemImpl {
        fun onClickYes(dialog: Dialog)
        fun onClickNo(dialog: Dialog)
    }
}