package com.app.rudo.view.lockdetails.iccard

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.model.APIError
import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import com.app.rudo.utils.NoResponseException
import kotlinx.coroutines.launch

class IcCardViewModel(
    private val repository: LockDetailRepository
) : ViewModel() {
    private var listner: IccardImpl? = null
    var name: String? = null
    var lockId: Int? = null
    //var keyData: KeyData? = null
    var lock: Lock? = null
    var startDate: Long = 0
    var endDate: Long = 0
    var type: String? = null
    fun registerListner(iccardImpl: IccardImpl) {
        listner = iccardImpl
    }

    fun onClickAdd(view: View) {
        // listner?.onStated()
        // val args = bundleOf("lock" to lockId)

    }

    fun onClickDelete(view: View) {
        listner?.onStated()
        listner?.onClickDelete(type!!)
    }

    fun getICCardList() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            icCardListFromOpenTTApi()
        } else {
            icCardListFromYutu()
        }
    }

    fun getFingerprintList() {
        listner?.onStated()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            fingerprintListFromOpenTT()
        } else {
            fingerprintListFromYutu()
        }
    }

    private fun fingerprintListFromYutu() {
        lock?.let {
            listner?.onStated()
            viewModelScope.launch {
                try {
                    val result = repository.getFingerprintList(lock?.lockId!!)
                    result?.let {
                        listner?.hideProgress()
                        if (!it.list.isNullOrEmpty()) {
                            listner?.fingerPrintList(result.list!!)
                        }else{
                            it.errmsg?.let {
                                listner?.onFailure(it!!)
                            }
                        }
                    }
                } catch (e: ApiException) {
                    listner?.onFailure(e.message!!)
                } catch (e: NoInternetException) {
                    listner?.onFailure(e.message!!)
                }
            }
        }
    }

    private fun fingerprintListFromOpenTT() {
        lock?.let {
            listner?.onStated()
            viewModelScope.launch {
                try {
                    val result = repository.getFingerprintListOpenTTApi(lock?.lockId!!)
                    result?.let {
                        listner?.hideProgress()
                        if (!it.list.isNullOrEmpty()) {
                            listner?.fingerPrintList(result.list!!)
                        }else{
                            it.errmsg?.let {
                                listner?.onFailure(it!!)
                            }
                        }
                    }
                } catch (e: ApiException) {
                    listner?.onFailure(e.message!!)
                } catch (e: NoInternetException) {
                    listner?.onFailure(e.message!!)
                }
            }
        }
    }

    private fun icCardListFromYutu() {
        lock?.let {
            listner?.onStated()
            viewModelScope.launch {
                try {
                    val result = repository.getICCardList(lock?.lockId!!)
                    result?.let {
                        listner?.hideProgress()
                        if (!it.list.isNullOrEmpty()) {
                            listner?.onSuccess(result.list!!)
                        }else{
                            it.errmsg?.let {
                                listner?.onFailure(it!!)
                            }
                        }
                    }
                } catch (e: ApiException) {
                    listner?.onFailure(e.message!!)
                } catch (e: NoInternetException) {
                    listner?.onFailure(e.message!!)
                }
            }
        }
    }

    private fun icCardListFromOpenTTApi() {
        lock?.let {
            listner?.onStated()
            viewModelScope.launch {
                try {
                    val result = repository.getICCardListOpenTTApi(lock?.lockId!!)
                    result?.let {
                        listner?.hideProgress()
                        if (!it.list.isNullOrEmpty()) {
                            listner?.onSuccess(result.list!!)
                        }else{
                            it.errmsg?.let {
                                listner?.onFailure(it!!)
                            }
                        }
                    }
                } catch (e: ApiException) {
                    listner?.onFailure(e.message!!)
                } catch (e: NoInternetException) {
                    listner?.onFailure(e.message!!)
                }
            }
        }
    }

    fun onClickOk(view: View) {
        if (name.isNullOrEmpty()) {
            listner?.onFailure("Empty Name")
            return
        }
        listner?.onStated()
        listner?.onClickOkButton()
    }

    fun uploadIcCardData(cardNumber: String) {
        if (cardNumber.isEmpty()) {
            listner?.onFailure("Card Number is empty")
            return
        }

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            uploadIcCardDataToOpenTTApi(cardNumber)
        } else {
            uploadIcCardDataToYutu(cardNumber)
        }

    }

    private fun uploadIcCardDataToYutu(cardNumber: String) {
        listner?.onStated()
        viewModelScope.launch {
            try {
                val response = repository.addIcCard(
                    lock?.lockId!!,
                    cardNumber,
                    name!!,
                    startDate,
                    endDate
                )
                response?.let {
                    if (it.errmsg.isNullOrEmpty()) {
                        listner?.addCardSuccess(it.cardId!!,"Card")
                    } else {
                        listner?.onFailure(it.errmsg)
                    }
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    private fun uploadIcCardDataToOpenTTApi(cardNumber: String) {
        viewModelScope.launch {
            try {
                val response = repository.addIcCardOpenTTApi(
                    lock?.lockId!!,
                    cardNumber,
                    name!!,
                    startDate,
                    endDate
                )
                response?.let {
                    if (it.errmsg.isNullOrEmpty()) {
                        listner?.addCardSuccess(it.cardId!!,"Card")
                    } else {
                        listner?.onFailure(it.errmsg)
                    }
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    fun addFingerprint(fingerprintNumber: String) {
        if (name.isNullOrEmpty()) {
            listner?.onFailure("Empty Name")
            return
        }
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            addFingerPrintOpenTTApi(fingerprintNumber)
        } else {
            addFingerPrintYutu(fingerprintNumber)
        }

    }

    private fun addFingerPrintYutu(fingerprintNumber: String) {
        try {
            viewModelScope.launch {
                val response = repository.addFingerprint(
                    lock?.lockId!!,
                    fingerprintNumber,
                    name!!,
                    startDate,
                    endDate
                )
                response?.let {
                    if (it.errmsg.isNullOrEmpty()) {
                        listner?.addCardSuccess(it.fingerprintId!!,"Fingerprint")
                    } else {
                        listner?.onFailure(it.errmsg)
                    }
                }
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

    private fun addFingerPrintOpenTTApi(fingerprintNumber: String) {
        try {
            viewModelScope.launch {
                val response = repository.addFingerprintOpenTT(
                    lock?.lockId!!,
                    fingerprintNumber,
                    name!!,
                    startDate,
                    endDate
                )
                response?.let {
                    if (it.errmsg.isNullOrEmpty()) {
                        listner?.addCardSuccess(it.fingerprintId!!,"Fingerprint")
                    } else {
                        listner?.onFailure(it.errmsg)
                    }
                }
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

    fun clearIcCard(){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            clearIcCardOpenTTApi()
        }else{
            clearIcCardYutu()
        }
    }

    private fun clearIcCardYutu() {
        try {
            viewModelScope.launch {
                val response = repository.clearIcCard(lock?.lockId!!)
                response?.let {
                    if(it.errcode != null && it.errcode == 0){
                        getICCardList()
                    }else{
                        listner?.onFailure(it.errmsg!!)
                    }
                }
            }
        }catch (e: NoResponseException){
            val sp = (APIError::class.java).cast(e)
            listner?.onFailure(sp?.error!!)

        } catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }

    private fun clearIcCardOpenTTApi() {
        try {
            viewModelScope.launch {
                val response = repository.clearIcCardOpenTT(lock?.lockId!!)
                response?.let {
                    if(it.errcode != null && it.errcode == 0){
                        getICCardList()
                    }else{
                        listner?.onFailure(it.errmsg!!)
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }
    fun clearFingerprint(){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            clearFingerprintOpenTTApi()
        }else{
            clearFingerprintYutu()
        }
    }

    private fun clearFingerprintYutu() {
        try {
            viewModelScope.launch {
                val response = repository.clearFingerprint(lock?.lockId!!)
                response?.let {
                    if(it.errcode != null && it.errcode == 0){
                        getICCardList()
                    }else{
                        listner?.onFailure(it.errmsg!!)
                    }
                }
            }
        }catch (e: NoResponseException){
            val sp = (APIError::class.java).cast(e)
            listner?.onFailure(sp?.error!!)

        } catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }

    private fun clearFingerprintOpenTTApi() {
        try {
            viewModelScope.launch {
                val response = repository.clearFingerprintOpenTT(lock?.lockId!!)
                response?.let {
                    if(it.errcode != null && it.errcode == 0){
                        getICCardList()
                    }else{
                        listner?.onFailure(it.errmsg!!)
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }

    fun deleteIcCard(iccardData: IccardData){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            deleteIcCardOpenTT(iccardData)
        }else{
            deleteIcCardYutu(iccardData)
        }
    }

    private fun deleteIcCardOpenTT(iccardData: IccardData) {
        try {
            viewModelScope.launch {
                val result = repository.deleteIcCardOpneTT(iccardData.lockId!!,iccardData.cardId!!)
                result.let {
                    if (result.errcode == 0){
                        getICCardList()
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }

    private fun deleteIcCardYutu(iccardData: IccardData) {
        try {
            viewModelScope.launch {
                val result = repository.deleteIcCard(iccardData.lockId!!,iccardData.cardId!!)
                result.let {
                    if (result.errcode == 0){
                        getICCardList()
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }
    fun deleteFingerprint(fingerprintData: FingerprintData){
        if(BuildConfig.IS_OPENTT_URL_CALL){
            deleteFingerprintOpenTT(fingerprintData)
        }else{
            deleteFingerprintYutu(fingerprintData)
        }
    }
    private fun deleteFingerprintOpenTT(fingerprintData: FingerprintData) {
        try {
            viewModelScope.launch {
                val result = repository.deleteFingerprintOpneTT(fingerprintData.lockId!!,fingerprintData.fingerprintId!!)
                result.let {
                    if (result.errcode == 0){
                        getFingerprintList()
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }

    private fun deleteFingerprintYutu(fingerprintData: FingerprintData) {
        try {
            viewModelScope.launch {
                val result = repository.deleteFingerprint(fingerprintData.lockId!!,fingerprintData.fingerprintId!!)
                result.let {
                    if (result.errcode == 0){
                        getFingerprintList()
                    }
                }
            }
        }catch (e:ApiException){
            listner?.onFailure(e.message!!)
        }catch (e:NoInternetException){
            listner?.onFailure(e.message!!)
        }
    }
}