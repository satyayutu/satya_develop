package com.app.rudo.view.addlock

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.repository.HomeRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch

class AddLockViewModel(
    private val repository: HomeRepository,
    private val pref: AppPrefrences
) : ViewModel() {
    private var listner: AddLockImpl? = null
    var deviceName: String? = null
    var lockData: String? = null
    fun registerListner(listnerr: AddLockImpl) {
        listner = listnerr
    }

    fun initLock() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            initLockOpenTTApi()
        } else {
            initLockYutuApi()
        }
    }

    fun OnClickSetName(view: View) {
       // val decoded: String = URLDecoder.decode(lockData, "UTF-8")
       // lockData = decoded
        listner?.onStarted()
        if (deviceName.isNullOrEmpty()) {
            listner?.onFailure("Name is Empty")
            return
        }
        initLock()
    }

    private fun initLockYutuApi() {

        viewModelScope.launch {
            try {
                val result = repository.initLock(lockData!!, deviceName!!)
                result?.let {
                    listner?.onSuccess()
                }

            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    private fun initLockOpenTTApi() {
        viewModelScope.launch {
            try {
                val result = repository.initLockOpenTTApi(lockData!!, deviceName!!)
                result?.let {
                    if (result.lockId != 0) {
                        listner?.onSuccess()
                    }else{
                        listner?.onFailure("Failure")
                    }
                }

            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }
}