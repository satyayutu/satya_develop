package com.app.rudo.view.lockdetails.iccard

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentIcPermanentBinding
import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.AddFingerprintCallback
import com.ttlock.bl.sdk.callback.AddICCardCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_ic_permanent.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 12-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AddIcPermanentFragment(private val lock: Lock, private val type: String) : Fragment(),
    KodeinAware, IccardImpl,
    DialogUitls.OnClickDialogItemImpl {
    override val kodein: Kodein by kodein()
    private val factory: IccardViewModelFactory by instance<IccardViewModelFactory>()
    private var viewmodel: IcCardViewModel? = null
    private var dialog: AlertDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentIcPermanentBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_ic_permanent, container
            , false
        )
        viewmodel = ViewModelProvider(this, factory).get(IcCardViewModel::class.java)
        binding.ivp = viewmodel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (type.equals("Fingerprint", ignoreCase = true)) {
            (activity as AppCompatActivity).supportActionBar?.title = "Fingerprint"
        }
        viewmodel?.registerListner(this)
        viewmodel?.lock = lock
    }

    override fun hideProgress() {
        progress_bar_icp.hide()
    }

    override fun onStated() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar_icp.show()
    }

    override fun onSuccess(list: List<IccardData>) {
        progress_bar_icp.hide()
    }

    override fun onFailure(message: String) {
        progress_bar_icp.hide()
        view?.rootView?.snackbar(message)
    }

    override fun addCardSuccess(cardId: Int, type:String) {
        edtEkeyName.text = null
        progress_bar_icp.hide()
        dialog = showAlertDialog {
            eText.text = cardId.toString()
            txtMessage.text = type+" id :"
            btnClickListener {
                dialog?.cancel()
            }
        }
        dialog?.setCancelable(false)
        dialog?.show()
       /* DialogUitls.showDialog(
            requireContext(),
            "Alert",
            cardId.toString(),
            true,
            false,
            this@AddIcPermanentFragment
        )*/

    }

    override fun fingerPrintList(list: List<FingerprintData>) {

    }

    override fun onClickDelete(type: String) {

    }

    override fun onClickOkButton() {
        if (type.isNotEmpty() && type == "Fingerprint") {
            TTLockClient.getDefault().addFingerprint(
                0,
                0,
                lock.lockData,
                lock.lockMac,
                object : AddFingerprintCallback {
                    override fun onEnterAddMode(totalCount: Int) {
                        view?.rootView?.snackbar("==put your fingerprint on lock=$totalCount")
                    }

                    override fun onCollectFingerprint(currentCount: Int) {
                        view?.rootView?.snackbar("==currentCount is $currentCount")
                    }

                    override fun onAddFingerpintFinished(fingerprintNum: Long) {
                        viewmodel?.addFingerprint(fingerprintNum.toString())
                    }

                    override fun onFail(error: LockError) {
                        progress_bar_icp.hide()
                        view?.rootView?.snackbar(error.errorMsg)
                    }
                })
        } else {
            TTLockClient.getDefault().addICCard(
                0,
                0,
                lock.lockData,
                lock.lockMac,
                object : AddICCardCallback {
                    override fun onEnterAddMode() {
                        view?.rootView?.snackbar("-you can put ic card on lock now-")
                    }

                    override fun onAddICCardSuccess(cardNum: Long) {
                        // view?.rootView?.snackbar("card is added to lock -$cardNum")
                        viewmodel?.uploadIcCardData(cardNum.toString())
                    }

                    override fun onFail(error: LockError) {
                        progress_bar_icp.hide()
                        view?.rootView?.snackbar(error.errorMsg)
                    }
                })
        }
    }

    override fun onClickYes(dialog: Dialog) {
        dialog.cancel()
    }

    override fun onClickNo(dialog: Dialog) {

    }
}