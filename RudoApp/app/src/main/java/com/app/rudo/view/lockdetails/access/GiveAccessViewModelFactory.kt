package com.app.rudo.view.lockdetails.access

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.repository.LockDetailRepository

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GiveAccessViewModelFactory (
    private val repository: LockDetailRepository

) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GiveAccessViewModel(repository) as T
    }
}