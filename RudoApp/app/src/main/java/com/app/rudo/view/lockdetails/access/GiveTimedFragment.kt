package com.app.rudo.view.lockdetails.access

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGiveTimedBinding
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_give_timed.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GiveTimedFragment(private var lockId:Int): Fragment(),GiveAccessImpl, KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory:GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel:GiveAccessViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : FragmentGiveTimedBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_give_timed,
            container,
            false
        )
        viewModel = ViewModelProvider(this,factory).get(GiveAccessViewModel::class.java)
        viewModel?.registerListner(this)
        binding.timed = viewModel
        viewModel?.registerListner(this)
        viewModel?.lockId = lockId
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startTime =DateUtils.getCurrentDateTime("startTime")
        val endTime =DateUtils.getCurrentDateTime("endTime")
        stateTimeTimed.text = DateUtils.getFormatedDateAndTime(startTime)
        endDateTimed.text = DateUtils.getFormatedDateAndTime(endTime)
        viewModel?.startTime = startTime
        viewModel?.endTime = endTime
        stateTimeTimed.setOnClickListener{
            DateUtils.getTimePicker(stateTimeTimed,requireContext())
        }
        endDateTimed.setOnClickListener{
            DateUtils.getTimePicker(endDateTimed,requireContext())
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progressBarGiveTimed.show()
    }

    override fun onSuccess() {
        edtRecipients.setText("")
        edtEkeyName.setText("")
        progressBarGiveTimed.hide()
        view?.rootView?.snackbar("Successfully share the eAccess")
    }

    override fun onFailure(message: String) {
        progressBarGiveTimed.hide()
        view?.rootView?.snackbar(message)
    }
}