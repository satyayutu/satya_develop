package com.app.rudo.view.settings

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentSettingsBinding
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.utils.DialogUitls
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import com.app.rudo.view.auth.LoginActivity
import kotlinx.android.synthetic.main.fragment_settings.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 19-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class SettingsFragment : Fragment(), KodeinAware, SettingsImpl, DialogUitls.OnClickDialogItemImpl {
    override val kodein: Kodein by kodein()
    val factory: SettingViewmodelFactory by instance<SettingViewmodelFactory>()
    var viewModel: SettingViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSettingsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        viewModel = ViewModelProvider(this, factory).get(SettingViewModel::class.java)
        binding.settingvm = viewModel
        viewModel?.registerListner(this)
        return binding.root
    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onLogout() {
        progress_bar.hide()
        Intent(requireContext(), LoginActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            requireContext().startActivity(it)
            requireActivity().finish()
        }
    }

    override fun onDeleteOntion() {
        DialogUitls.showDialog(
            requireContext(),
            "Delete Account",
            "Do you want to delete account?",
            true,
            true,
            this

        )
    }

    override fun onDeleteOntion(lockDetails: LockDetails) {

    }

    override fun onError(message: String) {
        view?.rootView?.snackbar(message)
    }

    override fun onClickYes(dialog: Dialog) {
        viewModel?.deleteUser()
    }

    override fun onClickNo(dialog: Dialog) {
        dialog?.cancel()
    }

}