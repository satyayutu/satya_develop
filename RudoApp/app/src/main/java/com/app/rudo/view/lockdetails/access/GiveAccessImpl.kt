package com.app.rudo.view.lockdetails.access

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface GiveAccessImpl {

    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
}