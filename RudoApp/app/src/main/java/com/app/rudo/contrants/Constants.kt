package com.app.rudo.contrants

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class Constants {
    companion object {
        const val PREFIX_USERNAME = "rudo_"
        const val Onetime = 1
        const val Permanent = 2
        const val Period = 3
        var LOCK_COUNT = 0

        const val eKeyNormal = "110401"
        const val eKeyReceiving = "110402"
        const val eKeyFrozen = "110405"
        const val eKeyDeleted = "110408"
        const val eKeyReset = "110410"
        var LOCKTIME = 0L
        var STARTTIME = 0L
        var ENDTIME = 0L
    }
}