package com.app.rudo.view.home.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.model.eaccess.EAccessKeyList
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.HomeRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch

class HomeViewModel(
    private val repository: HomeRepository,
    private val pref: AppPrefrences
) : ViewModel() {

    private val _lockList = MutableLiveData<List<Lock>>()
    private val _lockKeyList = MutableLiveData<List<EAccessKeyList>>()
    val lockList: LiveData<List<Lock>> = _lockList
    val lockKeyList: LiveData<List<EAccessKeyList>> = _lockKeyList
    var tempLockList: List<Lock>? = emptyList()

    // get() = _lockList

    private var homeListnerImpl: HomeListnerImpl? = null
    fun registerListner(homeListnerImpl: HomeListnerImpl) {
        this.homeListnerImpl = homeListnerImpl
    }

    fun getLockList() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getLockListFromOpenTTApi()
        } else {
            getLockListFromYutu()
        }


    }

    private fun getLockListFromYutu() {
        homeListnerImpl?.onStarted()
        viewModelScope.launch {
            try {
                val result = repository.getLockList()
                result.let {
                    if (it.list != null) {
                        if (it.list.size > 0) {
                            tempLockList = it.list
                            _lockList.value = it.list
                            homeListnerImpl?.onSuccess()
                        } else {
                            homeListnerImpl?.onFailure("No Lock is added")
                        }
                    } else {
                        it.errmsg?.let {
                            homeListnerImpl?.onFailure(it)
                        }
                    }
                }

            } catch (e: ApiException) {
                homeListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                homeListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun getLockListFromOpenTTApi() {
        viewModelScope.launch {
            try {
                val result = repository.getLockListFromOpenTTApi()
                result.let {
                    tempLockList = result.list
                    _lockList.value = it.list
                }
            } catch (e: ApiException) {
                homeListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                _lockList.value = pref.getLockData().list
                // homeListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    fun getEKeyList() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getEkeyListOpenTT()
        } else {

        }
    }

    private fun getEkeyListOpenTT() {
        viewModelScope.launch {
            try {
                val result = repository.getEkeyList()
                result.let {

                  /*  val lock = Lock(
                        0,
                        result.electricQuantity,
                        result.keyboardPwdVersion,
                        result.lockAlias,
                        result.lockId,
                        result.noKeyPwd,
                        result.lockMac,
                        result.lockName,
                        result.specialValue,
                        result.lockData
                    )
                    tempLockList?.toMutableList()?.add(lock)*/
                    if(result.list.isNotEmpty()){
                        _lockKeyList.value = result.list
                    }else{
                        homeListnerImpl?.onFailure("No Lock is Added")
                    }

                }
            } catch (e: ApiException) {
                homeListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                _lockList.value = pref.getLockData().list
                // homeListnerImpl?.onFailure(e.message!!)
            }
        }
    }

}