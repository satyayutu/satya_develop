package com.app.rudo.model

import com.app.rudo.model.eaccess.EAccessKeyList

data class LockKeyListMode(
    val list: List<EAccessKeyList>,
    val total: Int?,
    val page: Int?,
    val pageNo:Int?,
    val pageSize:Int?
)
