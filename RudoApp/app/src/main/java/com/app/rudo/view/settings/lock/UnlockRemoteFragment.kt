package com.app.rudo.view.settings.lock

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentUnlockRemoteBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.view.settings.SettingViewModel
import com.app.rudo.view.settings.SettingViewmodelFactory
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.GetRemoteUnlockStateCallback
import com.ttlock.bl.sdk.callback.SetRemoteUnlockSwitchCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_unlock_remote.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 02-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class UnlockRemoteFragment : Fragment(), KodeinAware {
    private val REQUEST_PERMISSION_REQ_CODE = 11
    override val kodein: Kodein by kodein()
    val factory: SettingViewmodelFactory by instance<SettingViewmodelFactory>()
    var viewModel: SettingViewModel? = null
    var lock: Lock? = null
    var unlockStatus: Boolean? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentUnlockRemoteBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_unlock_remote, container, false)
        viewModel = ViewModelProvider(this, factory).get(SettingViewModel::class.java)
        binding.remote = viewModel
        //viewModel?.registerListner(this)


        arguments?.let {
            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            viewModel?.lock = lock
            // binding.remote = lockdetails
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pdUnlock.show()
        ensureBluetoothIsEnabled()
        requestPermision()
        getRemoteUnlock()
        onClickButton()
    }

    private fun ensureBluetoothIsEnabled() {
        if (!TTLockClient.getDefault().isBLEEnabled(context)) {
            TTLockClient.getDefault().requestBleEnable(activity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }

    private fun onClickButton() {
        btnTurnCurrentMode.setOnClickListener {
            pdUnlock.show()
            setRemoteUnlock()
        }
    }

    private fun setRemoteUnlock() {
        val status: Boolean?
        if (unlockStatus!!) {
            status = false
        } else {
            status = true
        }
        //status = !unlockStatus!!
        TTLockClient.getDefault().setRemoteUnlockSwitchState(status, lock?.lockData, lock?.lockMac,
            object : SetRemoteUnlockSwitchCallback {
                override fun onFail(error: LockError?) {

                }

                override fun onSetRemoteUnlockSwitchSuccess(specialValue: Int) {
                    pdUnlock.hide()
                    getRemoteUnlock()
                }

            })
    }

    private fun getRemoteUnlock() {
        //true – on, false – off

        TTLockClient.getDefault().getRemoteUnlockSwitchState(lock?.lockData, lock?.lockMac,
            object : GetRemoteUnlockStateCallback {
                override fun onFail(error: LockError?) {

                }

                @SuppressLint("SetTextI18n")
                override fun onGetRemoteUnlockSwitchStateSuccess(enabled: Boolean) {
                    pdUnlock.hide()
                    unlockStatus = enabled
                    val status: String?
                    if (enabled) {
                        status = "ON"
                        btnTurnCurrentMode.text = "Turn OFF"
                    } else {
                        status = "OFF"
                        btnTurnCurrentMode.text = "Turn ON"
                    }
                    txtCurrentMode.text = status

                }

            })
        /*public void getRemoteUnlockSwitchState(
            String lockData,
            String lockMac,
            GetRemoteUnlockStateCallback callback)*/
    }

    private fun requestPermision() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
            return
        }
    }
}