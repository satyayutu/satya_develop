 package com.app.rudo.view.lockdetails.passcode

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.rudo.R
import com.app.rudo.model.locklist.Lock
import com.app.rudo.view.lockdetails.access.GiveAccessPageAdapter
import kotlinx.android.synthetic.main.fragment_give_access.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

 class GeneratePasscodeFragment : Fragment(),KodeinAware {

     private var lockId:Int? = null
     override val kodein:Kodein by kodein()
    // private var keyData:KeyData? = null
     private var lock:Lock? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_generate_passcode, container, false)

    }

     override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
         super.onViewCreated(view, savedInstanceState)
         arguments?.let {
             lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
         }
         setPageAdpter()
     }
     private fun setPageAdpter(){
         lock?.let {
             val adpter = GiveAccessPageAdapter(childFragmentManager)
             adpter.addFragment(GeneratePasscodePermanetFrgment(lock!!), "Permanent")
             adpter.addFragment(GeneratePasscodeOneTimeFragment(lock!!), "One Time")
             adpter.addFragment(GeneratePasscodeTimedFragment(lock!!), "Timed")
             viewpagerBase.adapter = adpter
         }
     }
 }


