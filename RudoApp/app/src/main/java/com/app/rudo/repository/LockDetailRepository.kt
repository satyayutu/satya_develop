package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.contrants.Constants
import com.app.rudo.model.ApiResponseModel
import com.app.rudo.model.PasscodeMainModel
import com.app.rudo.model.PasscodeModel
import com.app.rudo.model.ekeys.EAccessKeysList
import com.app.rudo.model.fingerprint.FingerprintList
import com.app.rudo.model.iccard.IccardList
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.model.recodes.RecordListModel
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.DateUtils
import java.net.URLDecoder

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class LockDetailRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {

    suspend fun getLockDetails(lockId: Int): LockDetails {
        val result = apiRequest {
            rudoApi.getLockDetails(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/detail"
            )!!
        }
        result?.let {
            result.lockAlias = URLDecoder.decode(result.lockAlias)
        }
        return result
    }

    suspend fun getLockDetailsFromOpenTTApi(lockId: Int): LockDetails {
        val result= apiRequest {
            rudoApi.getLockDetailsFromOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
        result?.let {
            it.lockAlias?.let {  result.lockAlias = URLDecoder.decode(result?.lockAlias) }

        }
        return result
    }

    suspend fun getKeyList(lockId: Int): KeyData {
        return apiRequest {
            rudoApi.getKeyList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/get"
            )!!
        }
    }

    suspend fun getKeyListFromOpenTTApi(lockId: Int): KeyData {
        return apiRequest {
            rudoApi.getKeyListFromOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun sendAccessKey(
        lockId: Int,
        receverName: String,
        name: String,
        stateDate: Long,
        endDate: Long,
        remoteEnabled: Int
    ): ApiResponseModel {
        return apiRequest {
            rudoApi.sendeAccessKey(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                receverName,
                name,
                stateDate,
                endDate,
                remoteEnabled,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/send"
            )!!
        }
    }

    suspend fun sendAccessKeyToOpenTTApi(
        lockId: Int,
        receverName: String,
        name: String,
        stateDate: Long,
        endDate: Long,
        remoteEnabled: Int
    ): ApiResponseModel {
        return apiRequest {
            rudoApi.sendeAccessKeyToOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                receverName,
                name,
                stateDate,
                endDate,
                remoteEnabled,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun addPassCode(
        lockId: Int,
        keyboardPwd: String,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long,
        addType: Int

    ): PasscodeModel {
        return apiRequest {
            rudoApi.addPassCode(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                keyboardPwd,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis(),
                addType,
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/add"
            )!!
        }
    }

    suspend fun addPassCodeOpenTTApi(
        lockId: Int,
        keyboardPwd: String,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long,
        addType: Int

    ): PasscodeModel {
        return apiRequest {
            rudoApi.addPassCodeOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                keyboardPwd,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis(),
                addType
            )!!
        }
    }

    suspend fun getPassCode(
        lockId: Int,
        keyboardPwdType: Int,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long
    ): PasscodeModel {
        return apiRequest {
            rudoApi.getPassCode(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                4,
                keyboardPwdType,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/get"
            )!!
        }
    }

    suspend fun getPassCodeOpenTTApi(
        lockId: Int,
        keyboardPwdType: Int,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long

    ): PasscodeModel {
        return apiRequest {
            rudoApi.getPassCodeOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                4,
                keyboardPwdType,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun getLockKey(lockId: Int): EAccessKeysList {
        val result = apiRequest {
            rudoApi.getLockKeyList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/listKey"
            )!!
        }
        result.let {
            it.list?.forEach {
                if (it.startDate == 0L && it.endDate == 0L) {
                    it.type = "Permanent"
                } else {
                    it.type =
                        DateUtils.getFormatedDateAndTime(it.startDate) + " -" + DateUtils.getFormatedDateAndTime(
                            it.endDate
                        )
                }

            }
            it.list?.filter { it.username!!.isNotEmpty() }?.forEach {
                if (it.username!!.contains("_")) {
                    it.username = it.username!!.split("_")[1]
                }
            }
            it.list?.filter { it.senderUsername!!.isNotEmpty() }?.forEach {
                if (it.senderUsername!!.contains("_")) {
                    it.senderUsername = it.senderUsername!!.split("_")[1]
                }
            }
        }
        return result

    }

    suspend fun getLockKeyOpenTTApi(lockId: Int): EAccessKeysList {
        val result = apiRequest {
            rudoApi.getLockKeyListOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis()
            )!!
        }
        result.let {

            it.list?.forEach {
                if (it.startDate == 0L && it.endDate == 0L) {
                    it.type = "Permanent"
                } else {
                    it.type =
                        DateUtils.getFormatedDateAndTime(it.startDate) + " -" + DateUtils.getFormatedDateAndTime(
                            it.endDate
                        )
                }

            }
            it.list?.filter { it.username!!.isNotEmpty() }?.forEach {
                if (it.username!!.contains("_")) {
                    it.username = it.username!!.split("_")[1]
                }
            }
            it.list?.filter { it.senderUsername!!.isNotEmpty() }?.forEach {
                if (it.senderUsername!!.contains("_")) {
                    it.senderUsername = it.senderUsername!!.split("_")[1]
                }
            }
        }
        return result
    }

    suspend fun getPassCodes(lockId: Int): PasscodeMainModel {
        return apiRequest {
            rudoApi.getPassCodeList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/listKeyboardPwd"
            )!!
        }

    }

    suspend fun getPassCodesOpenTTApi(lockId: Int): PasscodeMainModel {
        return apiRequest {
            rudoApi.getPassCodeListOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun getICCardListOpenTTApi(lockId: Int): IccardList {
        return apiRequest {
            rudoApi.getICCardListOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun getICCardList(lockId: Int): IccardList {
        return apiRequest {
            rudoApi.getICCardList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/list"
            )!!
        }
    }

    suspend fun getFingerprintList(lockId: Int): FingerprintList {
        return apiRequest {
            rudoApi.getFingerprintList(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/list"
            )!!
        }
    }

    suspend fun getFingerprintListOpenTTApi(lockId: Int): FingerprintList {
        return apiRequest {
            rudoApi.getFingerprintListOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                1,
                20,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun addIcCard(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addICCard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                1,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/add"
            )!!
        }
    }

    suspend fun addIcCardOpenTTApi(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addICCardOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                1,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun addFingerprint(
        lockId: Int,
        fingerprintNumber: String,
        fingerprintName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addFingerprint(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                fingerprintNumber,
                fingerprintName,
                startDate,
                endDate,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/add"
            )!!
        }
    }

    suspend fun addFingerprintOpenTT(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addFingerprintOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun uploadRecodes(lockId: Int, logs: String): ApiResponseModel {
        return apiRequest {
            rudoApi.unlockRecordsUpload(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                logs,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_URI,
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lockRecord/upload"
            )!!
        }
    }

    suspend fun uploadRecodesOpenTTApi(lockId: Int, logs: String): ApiResponseModel {
        return apiRequest {
            rudoApi.unlockRecordsUploadOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                logs,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun getUnloackRecods(lockId: Int): RecordListModel {
        val result= apiRequest {
            rudoApi.getUnlockRecords(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                0,
                0,
                1,
                100,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lockRecord/list"
            )!!
        }
        result.let {
            it.list.filter { it.username!!.isNotEmpty() }.forEach {
                if (it.username!!.contains("_")) {
                    it.username = it.username!!.split("_")[1]
                }
            }
        }
        return result
    }

    suspend fun getUnloackRecodsOpenTTApi(lockId: Int): RecordListModel {
        val result = apiRequest {
            rudoApi.getUnlockRecordsOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                0,
                0,
                1,
                100,
                System.currentTimeMillis()
            )!!
        }

        result.let {
            it.list.filter { it.username!!.isNotEmpty() }.forEach {
                if (it.username!!.contains("_")) {
                    it.username = it.username!!.split("_")[1]
                }
            }
        }
        return result
    }

    suspend fun clearIcCard(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/clear"
            )!!
        }
    }

    suspend fun clearIcCardOpenTT(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccardOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun clearFingerprint(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/clear"
            )!!
        }
    }

    suspend fun clearFingerprintOpenTT(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccardOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }
    suspend fun deletUser():ApiResponseModel{
        return apiRequest {
            rudoApi.deletUser(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                Constants.PREFIX_USERNAME+appPrefrences.getUserInfo()!!,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI+"/v3/user/delete"
            )!! }
    }
    suspend fun deletUserOpenTT():ApiResponseModel{
        return apiRequest {
            rudoApi.deletUserOpenTTApi(
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                appPrefrences.getUserInfo()!!,
                System.currentTimeMillis()
            )!! }
    }

    suspend fun deleteLock(lockId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteLock(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/lock/delete"
        )!! }
    }
    suspend fun deleteLockOpenTT(lockId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteLockOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            System.currentTimeMillis()
        )!! }
    }

    suspend fun deleteIcCard(lockId: Int,cardId:Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteIcCard(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            cardId,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/identityCard/delete"
        )!! }
    }
    suspend fun deleteIcCardOpneTT(lockId: Int,cardId:Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteIcCardOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            cardId,
            System.currentTimeMillis()
        )!! }
    }


    suspend fun deleteFingerprint(lockId: Int,fingerprintId:Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteFingerprint(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            fingerprintId,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/fingerprint/delete"
        )!! }
    }
    suspend fun deleteFingerprintOpneTT(lockId: Int,fingerprint:Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteFingerprintOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            fingerprint,
            System.currentTimeMillis()
        )!! }
    }


    suspend fun deleteeAccessKey(keyId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteeAccessKey(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            keyId,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/key/delete"
        )!! }
    }
    suspend fun deleteeAccessKeyOpneTT(keyId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteeAccessKeyOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            keyId,
            System.currentTimeMillis()
        )!! }
    }
    suspend fun deletePasscodeOpneTT(lockId: Int,passcodeId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteePasscodeOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            passcodeId,
            System.currentTimeMillis()
        )!! }
    }

    suspend fun deletePasscode(lockId: Int,passcodeId: Int):ApiResponseModel{
        return apiRequest { rudoApi.deleteePasscode(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            passcodeId,
            System.currentTimeMillis(),
            BuildConfig.REDIRECT_OPENTT_URI+"/v3/keyboardPwd/delete"
        )!! }
    }


    suspend fun authorizeEAccessOpenTT(lockId: Int,keyId: Int):ApiResponseModel{
        return apiRequest { rudoApi.authorizeEAccessOpenTT(
            BuildConfig.CLIENT_ID,
            appPrefrences.getAccessToken()!!,
            lockId,
            keyId,
            System.currentTimeMillis()
        )!! }
    }
}