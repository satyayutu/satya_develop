package com.app.rudo.view.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.repository.AuthRepository
import com.app.rudo.utils.AppPrefrences

@Suppress("UNCHECKED_CAST")
class AuthViewModelFactory(
    private val repository: AuthRepository,
    private val appPrefrences: AppPrefrences
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AuthViewModel(repository,appPrefrences) as T
    }
}