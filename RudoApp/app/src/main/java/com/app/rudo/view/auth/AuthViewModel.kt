package com.app.rudo.view.auth

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.model.AuthResponse
import com.app.rudo.repository.AuthRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.ttlock.bl.sdk.util.DigitUtil
import kotlinx.coroutines.launch


class AuthViewModel(
    private val repository: AuthRepository,
    private val prefrences: AppPrefrences
) : ViewModel() {

    private var authListnerImpl: AuthListnerImpl? = null
    var userName: String? = null//"8867830282"
    var password: String? = null//"pwd4TTLock"
    var confirmPassword: String? = null
    var loginButtonText: String? = "Sign In"
    //  fun getLoggedInUser() = repository.getUser()

    fun setListner(authListnerImpl: AuthListnerImpl) {
        this.authListnerImpl = authListnerImpl
    }

    fun onClickSignUp(view: View) {
        authListnerImpl?.navigateTo()
    }

    fun onClickSignIn(view: View) {
        authListnerImpl?.navigateTo()
    }

    fun onLoginButtonClick(view: View) {
        authListnerImpl?.onLoginButtonClick()


    }

    fun login() {
        if (userName.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid Mobile Number")
            return
        }
        if (password.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid PassWord")
            return
        }
        password = DigitUtil.getMD5(password!!.trim())
        authListnerImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            loginWithOpenTTServer()

        } else {
            loginWithYutuServer()
        }
    }

    fun forgetpassword() {
        if (userName.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid Mobile Number")
            return
        }
        if (password.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid PassWord")
            return
        }
        password = DigitUtil.getMD5(password!!.trim())
        authListnerImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            forgetPasswordWithOpenTTApi()
        } else {
            forgetPasswordWithYutuApi()
        }
    }


    private fun loginWithOpenTTServer() {
        viewModelScope.launch {
            try {
                val apiResponse = repository.userLoginWithOpenTT(userName!!, password!!)
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun loginWithYutuServer() {
        viewModelScope.launch {
            try {
                val apiResponse = repository.userLogin(userName!!, password!!)
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun loginReponseHandle(response: AuthResponse) {
        if (response.errmsg.isNullOrEmpty()) {
            response.access_token?.let {
                prefrences?.saveAccessToken(it)
                prefrences?.saveUserInfo(userName!!)
            }
            authListnerImpl?.onSuccess()

            //
        } else {
            authListnerImpl?.onFailure(response.errmsg)
        }
    }

    fun onSignUpButtonClick(view: View) {
        if (userName.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid Mobile Number")
            return
        }
        if (password.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid PassWord")
            return
        }
        if (password != confirmPassword) {
            authListnerImpl?.onFailure("Password not match")
            return
        }
        authListnerImpl?.onStarted()
        password = DigitUtil.getMD5(password)
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            signUpWithOpenTTServer(view)
        } else {
            signUpWithYutuServer(view)
        }
    }

    private fun signUpWithOpenTTServer(view: View) {
        viewModelScope.launch {
            try {
                val apiResponse =
                    repository.userSignupWithOpenTT(
                        userName!!,
                        password!!,
                        System.currentTimeMillis()
                    )
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun signUpWithYutuServer(view: View) {
        viewModelScope.launch {
            try {
                val apiResponse =
                    repository.userSignup(userName!!, password!!, System.currentTimeMillis())
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    fun forgetPasswordClick(view: View) {
        loginButtonText = "Reset"
        authListnerImpl?.onClickForgetPassword()
    }

    private fun forgetPasswordWithYutuApi() {
        viewModelScope.launch {
            try {
                val apiRequest = repository.forgetPassword(userName!!, password!!)
                apiRequest.let {
                    forgetPasswordResponsehandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun forgetPasswordWithOpenTTApi() {
        viewModelScope.launch {
            try {
                val apiRequest = repository.forgetPasswordWithOpenTT(userName!!, password!!)
                apiRequest.let {
                    forgetPasswordResponsehandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun forgetPasswordResponsehandle(authResponse: AuthResponse) {
        if (authResponse.errcode == 0) {
            loginButtonText = "Sign In"
            authListnerImpl?.onForgetPasswordSuccess()
        } else {
            authResponse.errcode.let {
                authListnerImpl?.onFailure(authResponse.errmsg!!)
            }
        }
    }
    // suspend fun saveLoggedInUser(user: User) = repository.saveUser(user)

}