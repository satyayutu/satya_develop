package com.app.rudo.view.lockdetails.passcode

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentGeneratePasscodeTimedBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_generate_passcode_timed.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class GeneratePasscodeTimedFragment (private var lock: Lock) : Fragment(),DialogUitls.OnClickDialogItemImpl, GeneratePasscodeImpl,
    KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory: GeneratePassCodeViewmodelFactory by instance<GeneratePassCodeViewmodelFactory>()
    private var viewModel: GeneratePasscodeViewModel? = null
    private var dialog: AlertDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentGeneratePasscodeTimedBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_generate_passcode_timed, container, false)
        viewModel = ViewModelProvider(this, factory).get(GeneratePasscodeViewModel::class.java)
         binding.timed = viewModel
        viewModel?.registerListner(this)
        viewModel?.setkeyboardPwdType(Constants.Period)
        viewModel?.lock = lock
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startTime =DateUtils.getCurrentDateTime("startTime")
        val endTime =DateUtils.getCurrentDateTime("endTime")
        stateTime.text = DateUtils.getFormatedDateAndTime(startTime)
        endDate.text = DateUtils.getFormatedDateAndTime(endTime)
        viewModel?.startTime = startTime
        viewModel?.endTime = endTime
        stateTime.setOnClickListener{
            DateUtils.getTimePicker(stateTime,requireContext())
        }
        endDate.setOnClickListener{
            DateUtils.getTimePicker(endDate,requireContext())
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar.show()
    }

    override fun onSuccess(message: String) {
        edtEkeyName.text = null
        progress_bar.hide()
        dialog = showAlertDialog{
            eText.text = message
            btnClickListener{
                dialog?.cancel()
            }
            imgShare.visibility = View.VISIBLE
            shareClickListener {
               // dialog?.dismiss()
                val sendIntent = Intent()
                sendIntent.setAction(Intent.ACTION_SEND)
                // sendIntent.setPackage("com.whatsapp")
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.setType("text/plain")
                startActivity(sendIntent)
            }
        }
        dialog?.setCancelable(false)

        dialog?.show()
       // DialogUitls.showDialog(requireContext(),"Alert",message,true,false,this@GeneratePasscodeTimedFragment)

    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }


    override fun onClickYes(dialog: Dialog) {

    }

    override fun onClickNo(dialog: Dialog) {

    }
}