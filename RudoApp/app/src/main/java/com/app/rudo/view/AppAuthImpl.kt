package com.app.rudo.view

/*
// Created by Satyabrata Bhuyan on 03-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface AppAuthImpl {
    fun onSuccess()
    fun onFailure()
}