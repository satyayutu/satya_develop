package com.app.rudo.view.lockdetails.access

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGivePermanetBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.hideKeyboard
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.fragment_give_permanet.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class GivePermanetFragment(private var lockId:Int) : Fragment(),GiveAccessImpl, KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory: GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel: GiveAccessViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentGivePermanetBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_give_permanet, container, false)
        viewModel = ViewModelProvider(this, factory).get(GiveAccessViewModel::class.java)
        binding.vm = viewModel
        viewModel?.registerListner(this)
        viewModel?.lockId = lockId
        viewModel?.startTime = 0L
        viewModel?.endTime = 0L
        return binding.root
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar.show()

    }

    override fun onSuccess() {
        edtRecipients.setText("")
        edtEkeyName.setText("")
        view?.rootView?.snackbar("Successfully share the eAccess")
        progress_bar.hide()
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }
}