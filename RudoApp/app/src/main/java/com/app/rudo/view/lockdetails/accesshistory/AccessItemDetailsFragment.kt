package com.app.rudo.view.lockdetails.accesshistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentAccessItemDetailsBinding
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.view.lockdetails.LockDeatilsImpl
import com.app.rudo.view.lockdetails.LockDetailsViewModel
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 11-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AccessItemDetailsFragment: Fragment(), KodeinAware,LockDeatilsImpl {

    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    var viewmodel: LockDetailsViewModel? = null
    var eAccesseModel : EAccessKey? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentAccessItemDetailsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_access_item_details, container, false)
        viewmodel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        viewmodel?.registerListner(this)
        arguments?.let {
            eAccesseModel = (EAccessKey::class.java).cast(arguments?.getSerializable("eaccessdetails"))
            viewmodel?.eAccessKey = eAccesseModel
            binding.eaccessviewmodel = viewmodel
            binding.eaccessdetails = eAccesseModel
        }
        return binding.root
    }

    override fun onStarted() {

    }

    override fun onSuccess() {

    }

    override fun onSuccesLockDetails(lockDetails: KeyData) {

    }

    override fun onFailure(message: String) {

    }

    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {
        view?.findNavController()?.popBackStack()
    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {

    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }
}