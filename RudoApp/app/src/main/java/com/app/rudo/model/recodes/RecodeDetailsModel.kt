package com.app.rudo.model.recodes

/*
// Created by Satyabrata Bhuyan on 17-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class RecodeDetailsModel(
    val lockId:	Int?,
    val recordType:Int?,
    val success:Int?,//	Is success: 0-No, 1-Yes
    var username:String?,
    val keyboardPwd:String?,	//Passcode,IC card number,or wristband address
    val lockDate:Long?,
    val serverDate:Long
)

