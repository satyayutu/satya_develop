package com.app.rudo.view.customersupport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import kotlinx.android.synthetic.main.fragment_faq.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.HashMap

/*
// Created by Satyabrata Bhuyan on 07-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class FAQFragment : Fragment(), KodeinAware {

    override val kodein: Kodein by kodein()
    private val factory: SupportViewModelFactory by instance<SupportViewModelFactory>()
    private lateinit var viewModel: SupportViewModel
    internal var adapter: ExpandableListAdapter? = null
    var titleList: List<String>? = null

    //val data: HashMap<String, List<String>> = HashMap()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_faq, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(SupportViewModel::class.java)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var listData = HashMap<String, List<String>>()
        val locks = ArrayList<String>()
        val keypad = ArrayList<String>()
        val passcode = ArrayList<String>()
        val mobileapp = ArrayList<String>()
        val others = ArrayList<String>()



        locks.add("Introduction")
        locks.add("Hoe to ADD Smart Lock?")
        locks.add("What are the Different Methods to Operate the Smart Lock?")
        locks.add("How do I adjust the clock on my smart lock?")



        keypad.add("Why do the keypad lights go out immediately after ‘activating’ the smart lock?")
        keypad.add("Why is the keypad flashing when operating the Smart Lock?")
        keypad.add("Why can’t the keypad be activated?")



        passcode.add("How long can a passcode be valid for?")
        passcode.add("What happens if the User and Smart Lock are in different Time Zones?")
        passcode.add("Why is my Passcode not working?")



        mobileapp.add("Is it possible to UNLOCK the Smart Lock using the Application when the Phone is not Connected to 3G/4G/Wi-Fi?")
        mobileapp.add("Why am I unable to Operate the Smart Lock using the Application on my Phone?")


        others.add("How can I Invalidate an eAccess, Fingerprint, Passcode or IC Card?")
        others.add("What is the GATEWAY used for?")

        listData.put("Locks", locks)
        listData.put("Keypad", keypad)
        listData.put("Passcode", passcode)
        listData.put("Mobile App", mobileapp)
        listData.put("Others",others)

        if (expandableListView != null) {
            titleList = ArrayList(listData.keys.reversed())
            adapter = CustomExpandableListAdapter(
                requireContext(),
                titleList as ArrayList<String>,
                listData
            )
            expandableListView!!.setAdapter(adapter)

            expandableListView!!.setOnGroupExpandListener {
                //groupPosition -> Toast.makeText(requireContext(), (titleList as ArrayList<String>)[groupPosition] + " List Expanded.", Toast.LENGTH_SHORT).show()
            }

            expandableListView!!.setOnGroupCollapseListener { groupPosition ->
                //Toast.makeText(requireContext(), (titleList as ArrayList<String>)[groupPosition] + " List Collapsed.", Toast.LENGTH_SHORT).show()
            }

            expandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                // Toast.makeText(applicationContext, "Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + listData[(titleList as ArrayList<String>)[groupPosition]]!!.get(childPosition), Toast.LENGTH_SHORT).show()
                false
            }
        }
    }

}