package com.app.rudo.view.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.drawer_header.view.*


/*
// Created by Satyabrata Bhuyan on 02-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class HomeActivity : AppCompatActivity() {
    var navController: NavController? = null
    var pref: AppPrefrences? = null
    var exit = 0
    var doubleBackToExitPressedOnce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(getColor(R.color.white))
        navController = Navigation.findNavController(this, R.id.fragment)
        NavigationUI.setupWithNavController(nav_view, navController!!)
        NavigationUI.setupActionBarWithNavController(this, navController!!, drawer_layout)
        val headerView = nav_view.inflateHeaderView(R.layout.drawer_header)
        pref = AppPrefrences(this)
        pref?.let {
            headerView.mobileNumber.text = pref?.getUserInfo()
        }
        itemClicked()

    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            Navigation.findNavController(this, R.id.fragment),
            drawer_layout
        )
    }

    override fun onNavigateUp(): Boolean {
        return super.onNavigateUp()
    }

    fun itemClicked() {
        nav_view.setNavigationItemSelectedListener {
            val id = it.itemId

            when (id) {
                R.id.addLock -> {
                    drawer_layout.closeDrawers()
                    navController?.navigate(R.id.addLockFragment)
                    return@setNavigationItemSelectedListener true
                }
                R.id.settings -> {
                    drawer_layout.closeDrawers()
                    navController?.navigate(R.id.settingsFragment)
                    return@setNavigationItemSelectedListener true
                }
                R.id.gateway -> {
                    drawer_layout.closeDrawers()
                    navController?.navigate(R.id.gatewayAccFragment)
                    return@setNavigationItemSelectedListener true
                }
                R.id.customerSupport -> {
                    drawer_layout.closeDrawers()
                    navController?.navigate(R.id.fragmentSupport)
                    return@setNavigationItemSelectedListener true
                }
                else -> return@setNavigationItemSelectedListener false
            }

        }
    }

    override fun onStart() {
        super.onStart()
        exit = 0
    }

    override fun onBackPressed() {
        val fragment = navController?.currentBackStackEntry?.destination?.label
        if (fragment?.equals("Lock Details")!! && Constants.LOCK_COUNT > 1) {
            super.onBackPressed()
            return
        } else if (!fragment?.equals("Home")!! && !fragment?.equals("Lock Details")) {
            super.onBackPressed()
            return
        }

        if (doubleBackToExitPressedOnce) {
            Intent(Intent.ACTION_MAIN).also {
                it.addCategory(Intent.CATEGORY_HOME)
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
                finishAffinity()
                finish()
            }
        } else {
            this.doubleBackToExitPressedOnce = true
            this.toast("Press again to exit")
            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

}