package com.app.rudo.view.auth

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.ActivityLoginBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.hideKeyboard
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import com.app.rudo.view.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : AppCompatActivity(), AuthListnerImpl, KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    var viewmodel: AuthViewModel? = null
    val REQUEST_PERMISSION_SMS = 12
    val REQUEST_PERMISSION_SMS_READ = 13
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel?.setListner(this)
        //viewmodel?.userName = "8867830282"
        //viewmodel?.password = "pwd4TTLock"
        appPermission()

    }

    override fun onStarted() {
        applicationContext.hideKeyboard(root_layout)
        progress_bar.show()
    }

    override fun onSuccess() {
        edit_text_email.text = null
        edit_text_password.text = null
        progress_bar.hide()
        Intent(this, HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(it)
            finish()
        }
    }

    override fun onForgetPasswordSuccess() {
        edit_text_email.text = null
        edit_text_password.text = null
        button_sign_in.text = "Sign In"
        viewmodel?.loginButtonText = "Sign In"
        progress_bar?.hide()
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        root_layout.snackbar(message)
        button_sign_in.text = "Sign In"
        viewmodel?.loginButtonText = "Sign In"
    }

    override fun onClickForgetPassword() {
        edit_text_email.text = null
        edit_text_password.text = null
        button_sign_in.text = "Reset"
        viewmodel?.loginButtonText = "Reset"

    }

    override fun navigateTo() {
        Intent(this, SignupActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(it)
        }
    }

    override fun onLoginButtonClick() {
        if (button_sign_in.text == "Reset") {
            viewmodel?.forgetpassword()
        } else {
            viewmodel?.login()
        }
    }

    private fun appPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.SEND_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.SEND_SMS),
                REQUEST_PERMISSION_SMS
            )
            return
        }
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_SMS),
                REQUEST_PERMISSION_SMS_READ
            )
            return
        }
    }
}
