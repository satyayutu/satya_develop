package com.app.rudo.view.settings.lock

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentAutoLockSettingsBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.app.rudo.view.settings.SettingViewModel
import com.app.rudo.view.settings.SettingViewmodelFactory
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.GetLockTimeCallback
import com.ttlock.bl.sdk.callback.SetAutoLockingPeriodCallback
import com.ttlock.bl.sdk.callback.SetLockTimeCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_auto_lock_settings.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber
import kotlin.time.ExperimentalTime

/*
// Created by Satyabrata Bhuyan on 03-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AutoLockFragment : Fragment(), KodeinAware {
    private val REQUEST_PERMISSION_REQ_CODE = 11
    override val kodein: Kodein by kodein()
    val factory: SettingViewmodelFactory by instance<SettingViewmodelFactory>()
    var viewModel: SettingViewModel? = null
    var lock: Lock? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAutoLockSettingsBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_auto_lock_settings,
                container,
                false
            )
        viewModel = ViewModelProvider(this, factory).get(SettingViewModel::class.java)

        if(viewModel?.lock != null){
            lock = viewModel?.lock
        }else{
            arguments?.let {
                lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
                viewModel?.lock = lock
                // binding.remote = lockdetails
            }
        }

        return binding.root
    }

    @ExperimentalTime
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ensureBluetoothIsEnabled()
        requestPermision()
        //getLockTime()

        buttonSave.setOnClickListener{
            progress_bar?.show()
            context?.hideKeyboard(view?.rootView!!)
            if(!TextUtils.isEmpty(txtCustomTime.text.toString())){
                autoLock()
            }else{
                view?.rootView?.snackbar("Please Enter Time")
            }

        }
    }

    private fun ensureBluetoothIsEnabled() {
        if (!TTLockClient.getDefault().isBLEEnabled(context)) {
            TTLockClient.getDefault().requestBleEnable(activity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        /**
         * BT service should be released before Activity finished.
         */
        TTLockClient.getDefault().stopBTService()
    }


    private fun setLockTime() {

        TTLockClient.getDefault().setLockTime(System.currentTimeMillis(), lock?.lockData,
            lock?.lockMac, object : SetLockTimeCallback {
                override fun onFail(error: LockError?) {

                }

                override fun onSetTimeSuccess() {
                    val seconds = (System.currentTimeMillis() / 1000) * 60

                    txtTime.text = seconds.toString() + "s"
                }

            })
    }

    @ExperimentalTime
    private fun getLockTime() {
        //true – on, false – off

        TTLockClient.getDefault().getLockTime(lock?.lockData, lock?.lockMac,
            object : GetLockTimeCallback {
                override fun onFail(error: LockError?) {

                }

                @SuppressLint("SetTextI18n")
                override fun onGetLockTimeSuccess(lockTimestamp: Long) {
                    val seconds = DateUtils.getSeconds(lockTimestamp)

                    txtTime.text = seconds

                }

            })
        /*public void getRemoteUnlockSwitchState(
            String lockData,
            String lockMac,
            GetRemoteUnlockStateCallback callback)*/
    }

    private fun requestPermision() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_REQ_CODE
            )
            return
        }
    }

    private fun autoLock(){
        TTLockClient.getDefault().setAutomaticLockingPeriod(
            txtCustomTime.text.toString().toInt(),
        lock?.lockData,
        lock?.lockMac,
        object : SetAutoLockingPeriodCallback{
            override fun onFail(p0: LockError?) {
                progress_bar?.hide()
                Timber.i("SetAutoLockingPeriodCallback Error %s",p0.toString())
                view?.rootView?.snackbar(p0?.errorMsg!!)
            }

            override fun onSetAutoLockingPeriodSuccess() {
                progress_bar?.hide()
                txtTime.text = txtCustomTime.text.toString()+"s"
            }
        })
    }
}