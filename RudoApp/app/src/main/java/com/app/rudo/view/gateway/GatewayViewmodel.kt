package com.app.rudo.view.gateway

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.repository.GatewayRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayViewmodel(
    private val repository: GatewayRepository,
    private val pref: AppPrefrences
) : ViewModel() {

    var listner: GatewayImpl? = null
    fun registerListner(mListner: GatewayImpl) {
        this.listner = mListner
    }

    fun getAccList() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getGatewayAccListOpenTT()
        } else {
            getGatewayAccLisYutu()
        }
    }

    fun OnClickAddButton(view: View) {
        view.findNavController().navigate(R.id.action_gatewayacc_to_addgateway)
    }

    private fun getGatewayAccLisYutu() {
        listner?.onStarted()
        try {
            viewModelScope.launch {
                val response = repository.getGatewayAccList()
                listner?.OnGatewayAccItem(response.list)
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

    private fun getGatewayAccListOpenTT() {
        try {
            viewModelScope.launch {
                val response = repository.getGatewayAccListOpenTT()
                listner?.OnGatewayAccItem(response.list)
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

}