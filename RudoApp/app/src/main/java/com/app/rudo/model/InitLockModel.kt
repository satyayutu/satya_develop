package com.app.rudo.model

data class InitLockModel(
    val keyId: Int,
    val lockId: Int
)